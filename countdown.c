///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   ./countdown (decimal value form -2147483646 to 2147483646)
//	 
//	 ./countdown 2147483646
//   
//	 Reference time: Mon Jan 18 05:14:06 PM HST 2038
//	 Years = [15]  Days = [328]  Hours = [22]  Minutes = [8]  Seconds = [27]
//	 Years = [15]  Days = [328]  Hours = [22]  Minutes = [8]  Seconds = [26]
//   Years = [15]  Days = [328]  Hours = [22]  Minutes = [8]  Seconds = [25]
//	 Years = [15]  Days = [328]  Hours = [22]  Minutes = [8]  Seconds = [24]
//	 Years = [15]  Days = [328]  Hours = [22]  Minutes = [8]  Seconds = [23]
//	 Years = [15]  Days = [328]  Hours = [22]  Minutes = [8]  Seconds = [22]
//   Years = [15]  Days = [328]  Hours = [22]  Minutes = [8]  Seconds = [21]
//
// @author Xiaokang Chen <xiaokang@hawaii.edu>
// @date   21 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>


void intruction() {

   printf( "Please enter a value form [-2147483646] to [2147483646]\n" );

}


int main( int argc, char* argv[] ) {

   if ( argc < 2 ) {
      
      intruction();
      return( EXIT_FAILURE);
   
   }


   if ( ( atoi( argv[1] ) < -2147483646 ) || ( atoi( argv[1] ) > 2147483646 ) ) {

       printf("Time out of index\n");
       intruction();
       return( EXIT_FAILURE );

   }

   time_t enterTime = atoi( argv[1] );
   time_t currentTime;

   //for testing purpose
   //struct tm * enterTimeStatus;
   //enterTimeStatus = localtime( &enterTime );
   //printf( "enterTime = %s", asctime( enterTimeStatus ) );
   //printf( "Years = [%d]  Days = [%d]  Hours = [%d]  Minutes = [%d]  Seconds = [%d]\n", enterTimeStatus->tm_year, enterTimeStatus->tm_yday, enterTimeStatus->tm_hour, enterTimeStatus->tm_min, enterTimeStatus->tm_sec );

   struct tm * printEnterTime;
   char   buffer[80];

   printEnterTime = localtime( &enterTime );
   
   strftime( buffer, 80, "Reference time: %a %b %d %r %Z %Y", printEnterTime );
   puts( buffer );
   
   //redefine tm value
   struct tm * timeStatus = { 0 };
   

   while( true ) {

      sleep( 1 );
      time( &currentTime );

      //for testing purpose
      //printf( "currentTime = [%d]\n", currentTime );
      //struct tm * currentTimeStatus;
      //currentTimeStatus = localtime( &currentTime );
      //printf( "currentTime = %s", asctime( currentTimeStatus ) );
      //printf( "Years = [%d]  Days = [%d]  Hours = [%d]  Minutes = [%d]  Seconds = [%d]\n", currentTimeStatus->tm_year, currentTimeStatus->tm_yday, currentTimeStatus->tm_hour, currentTimeStatus->tm_min, currentTimeStatus->tm_sec );

      time_t diffTime = enterTime - currentTime;

      if ( diffTime < 0 ) {

         diffTime  = -(diffTime);

      }

      //for testing purpose
      //printf( "diffTime = [%d]\n", diffTime );

      diffTime += -2147483646;

      //for testing purpose
      //printf( "diffTime = [%d]\n", diffTime );

      timeStatus = localtime( &diffTime );

      //for testing purpose
      //printf( "%s\n", asctime( timeStatus ) );
      
      timeStatus->tm_year -=   1;
      timeStatus->tm_yday -= 346;
      timeStatus->tm_hour -=  10;
      timeStatus->tm_min  -=  15;
      timeStatus->tm_sec  -=  54;
      
      if ( ( timeStatus->tm_yday ) < 0 ) {

         timeStatus->tm_year -=   1;
         timeStatus->tm_yday += 365;
     
      }
      
      if ( ( timeStatus->tm_hour ) < 0 ) {

         timeStatus->tm_yday  -=  1;
         timeStatus->tm_hour  += 24;

      }
     
      if ( ( timeStatus->tm_min < 0 ) ) {

         timeStatus->tm_hour -=  1;
         timeStatus->tm_min  += 60;

      }

      if ( ( timeStatus->tm_sec < 0 ) ) {
         
         timeStatus->tm_min -=  1;
         timeStatus->tm_sec += 60;

      }

      printf( "Years = [%d]  Days = [%d]  Hours = [%d]  Minutes = [%d]  Seconds = [%d]\n", timeStatus->tm_year, timeStatus->tm_yday, timeStatus->tm_hour, timeStatus->tm_min, timeStatus->tm_sec );

   }

}
